[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# home

hosting information about the OpenEngrVE open project, which is an effort to automate processes commonly used in engineering and adapt them to the current norm.


* @jorgemustaine jorgescalona escajorge@gmail.com (2018)
* @wendyulacio wendy ulacio wendyulacio@gmail.com (2018)
